// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: monster.proto
// </auto-generated>
#pragma warning disable 0414, 1591
#region Designer generated code

using grpc = global::Grpc.Core;

namespace Monster {
  public static partial class MonsterServices
  {
    static readonly string __ServiceName = "monster.MonsterServices";

    static void __Helper_SerializeMessage(global::Google.Protobuf.IMessage message, grpc::SerializationContext context)
    {
      #if !GRPC_DISABLE_PROTOBUF_BUFFER_SERIALIZATION
      if (message is global::Google.Protobuf.IBufferMessage)
      {
        context.SetPayloadLength(message.CalculateSize());
        global::Google.Protobuf.MessageExtensions.WriteTo(message, context.GetBufferWriter());
        context.Complete();
        return;
      }
      #endif
      context.Complete(global::Google.Protobuf.MessageExtensions.ToByteArray(message));
    }

    static class __Helper_MessageCache<T>
    {
      public static readonly bool IsBufferMessage = global::System.Reflection.IntrospectionExtensions.GetTypeInfo(typeof(global::Google.Protobuf.IBufferMessage)).IsAssignableFrom(typeof(T));
    }

    static T __Helper_DeserializeMessage<T>(grpc::DeserializationContext context, global::Google.Protobuf.MessageParser<T> parser) where T : global::Google.Protobuf.IMessage<T>
    {
      #if !GRPC_DISABLE_PROTOBUF_BUFFER_SERIALIZATION
      if (__Helper_MessageCache<T>.IsBufferMessage)
      {
        return parser.ParseFrom(context.PayloadAsReadOnlySequence());
      }
      #endif
      return parser.ParseFrom(context.PayloadAsNewBuffer());
    }

    static readonly grpc::Marshaller<global::Monster.Monster> __Marshaller_monster_Monster = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Monster.Monster.Parser));
    static readonly grpc::Marshaller<global::Common.MonId> __Marshaller_common_MonId = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Common.MonId.Parser));
    static readonly grpc::Marshaller<global::Common.UserId> __Marshaller_common_UserId = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Common.UserId.Parser));
    static readonly grpc::Marshaller<global::Monster.LockRequest> __Marshaller_monster_LockRequest = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Monster.LockRequest.Parser));
    static readonly grpc::Marshaller<global::Monster.ReleaseRequest> __Marshaller_monster_ReleaseRequest = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Monster.ReleaseRequest.Parser));
    static readonly grpc::Marshaller<global::Common.ResponseStatus> __Marshaller_common_ResponseStatus = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Common.ResponseStatus.Parser));
    static readonly grpc::Marshaller<global::Monster.MonsterList> __Marshaller_monster_MonsterList = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Monster.MonsterList.Parser));

    static readonly grpc::Method<global::Monster.Monster, global::Common.MonId> __Method_AddMon = new grpc::Method<global::Monster.Monster, global::Common.MonId>(
        grpc::MethodType.Unary,
        __ServiceName,
        "AddMon",
        __Marshaller_monster_Monster,
        __Marshaller_common_MonId);

    static readonly grpc::Method<global::Common.UserId, global::Monster.Monster> __Method_AddTestMon = new grpc::Method<global::Common.UserId, global::Monster.Monster>(
        grpc::MethodType.Unary,
        __ServiceName,
        "AddTestMon",
        __Marshaller_common_UserId,
        __Marshaller_monster_Monster);

    static readonly grpc::Method<global::Monster.LockRequest, global::Monster.Monster> __Method_LockMonster = new grpc::Method<global::Monster.LockRequest, global::Monster.Monster>(
        grpc::MethodType.Unary,
        __ServiceName,
        "LockMonster",
        __Marshaller_monster_LockRequest,
        __Marshaller_monster_Monster);

    static readonly grpc::Method<global::Monster.ReleaseRequest, global::Common.ResponseStatus> __Method_ReleaseMonster = new grpc::Method<global::Monster.ReleaseRequest, global::Common.ResponseStatus>(
        grpc::MethodType.Unary,
        __ServiceName,
        "ReleaseMonster",
        __Marshaller_monster_ReleaseRequest,
        __Marshaller_common_ResponseStatus);

    static readonly grpc::Method<global::Common.MonId, global::Monster.Monster> __Method_GetMonByID = new grpc::Method<global::Common.MonId, global::Monster.Monster>(
        grpc::MethodType.Unary,
        __ServiceName,
        "GetMonByID",
        __Marshaller_common_MonId,
        __Marshaller_monster_Monster);

    static readonly grpc::Method<global::Common.UserId, global::Monster.MonsterList> __Method_GetMonsByUserID = new grpc::Method<global::Common.UserId, global::Monster.MonsterList>(
        grpc::MethodType.Unary,
        __ServiceName,
        "GetMonsByUserID",
        __Marshaller_common_UserId,
        __Marshaller_monster_MonsterList);

    static readonly grpc::Method<global::Monster.Monster, global::Monster.Monster> __Method_UpdateMon = new grpc::Method<global::Monster.Monster, global::Monster.Monster>(
        grpc::MethodType.Unary,
        __ServiceName,
        "UpdateMon",
        __Marshaller_monster_Monster,
        __Marshaller_monster_Monster);

    static readonly grpc::Method<global::Common.MonId, global::Common.ResponseStatus> __Method_DeleteMon = new grpc::Method<global::Common.MonId, global::Common.ResponseStatus>(
        grpc::MethodType.Unary,
        __ServiceName,
        "DeleteMon",
        __Marshaller_common_MonId,
        __Marshaller_common_ResponseStatus);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::Monster.MonsterReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of MonsterServices</summary>
    [grpc::BindServiceMethod(typeof(MonsterServices), "BindService")]
    public abstract partial class MonsterServicesBase
    {
      public virtual global::System.Threading.Tasks.Task<global::Common.MonId> AddMon(global::Monster.Monster request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Monster.Monster> AddTestMon(global::Common.UserId request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Monster.Monster> LockMonster(global::Monster.LockRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Common.ResponseStatus> ReleaseMonster(global::Monster.ReleaseRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Monster.Monster> GetMonByID(global::Common.MonId request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Monster.MonsterList> GetMonsByUserID(global::Common.UserId request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Monster.Monster> UpdateMon(global::Monster.Monster request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Common.ResponseStatus> DeleteMon(global::Common.MonId request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Client for MonsterServices</summary>
    public partial class MonsterServicesClient : grpc::ClientBase<MonsterServicesClient>
    {
      /// <summary>Creates a new client for MonsterServices</summary>
      /// <param name="channel">The channel to use to make remote calls.</param>
      public MonsterServicesClient(grpc::ChannelBase channel) : base(channel)
      {
      }
      /// <summary>Creates a new client for MonsterServices that uses a custom <c>CallInvoker</c>.</summary>
      /// <param name="callInvoker">The callInvoker to use to make remote calls.</param>
      public MonsterServicesClient(grpc::CallInvoker callInvoker) : base(callInvoker)
      {
      }
      /// <summary>Protected parameterless constructor to allow creation of test doubles.</summary>
      protected MonsterServicesClient() : base()
      {
      }
      /// <summary>Protected constructor to allow creation of configured clients.</summary>
      /// <param name="configuration">The client configuration.</param>
      protected MonsterServicesClient(ClientBaseConfiguration configuration) : base(configuration)
      {
      }

      public virtual global::Common.MonId AddMon(global::Monster.Monster request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return AddMon(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Common.MonId AddMon(global::Monster.Monster request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_AddMon, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Common.MonId> AddMonAsync(global::Monster.Monster request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return AddMonAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Common.MonId> AddMonAsync(global::Monster.Monster request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_AddMon, null, options, request);
      }
      public virtual global::Monster.Monster AddTestMon(global::Common.UserId request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return AddTestMon(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Monster.Monster AddTestMon(global::Common.UserId request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_AddTestMon, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Monster.Monster> AddTestMonAsync(global::Common.UserId request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return AddTestMonAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Monster.Monster> AddTestMonAsync(global::Common.UserId request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_AddTestMon, null, options, request);
      }
      public virtual global::Monster.Monster LockMonster(global::Monster.LockRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return LockMonster(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Monster.Monster LockMonster(global::Monster.LockRequest request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_LockMonster, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Monster.Monster> LockMonsterAsync(global::Monster.LockRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return LockMonsterAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Monster.Monster> LockMonsterAsync(global::Monster.LockRequest request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_LockMonster, null, options, request);
      }
      public virtual global::Common.ResponseStatus ReleaseMonster(global::Monster.ReleaseRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return ReleaseMonster(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Common.ResponseStatus ReleaseMonster(global::Monster.ReleaseRequest request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_ReleaseMonster, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Common.ResponseStatus> ReleaseMonsterAsync(global::Monster.ReleaseRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return ReleaseMonsterAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Common.ResponseStatus> ReleaseMonsterAsync(global::Monster.ReleaseRequest request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_ReleaseMonster, null, options, request);
      }
      public virtual global::Monster.Monster GetMonByID(global::Common.MonId request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return GetMonByID(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Monster.Monster GetMonByID(global::Common.MonId request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_GetMonByID, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Monster.Monster> GetMonByIDAsync(global::Common.MonId request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return GetMonByIDAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Monster.Monster> GetMonByIDAsync(global::Common.MonId request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_GetMonByID, null, options, request);
      }
      public virtual global::Monster.MonsterList GetMonsByUserID(global::Common.UserId request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return GetMonsByUserID(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Monster.MonsterList GetMonsByUserID(global::Common.UserId request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_GetMonsByUserID, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Monster.MonsterList> GetMonsByUserIDAsync(global::Common.UserId request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return GetMonsByUserIDAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Monster.MonsterList> GetMonsByUserIDAsync(global::Common.UserId request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_GetMonsByUserID, null, options, request);
      }
      public virtual global::Monster.Monster UpdateMon(global::Monster.Monster request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return UpdateMon(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Monster.Monster UpdateMon(global::Monster.Monster request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_UpdateMon, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Monster.Monster> UpdateMonAsync(global::Monster.Monster request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return UpdateMonAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Monster.Monster> UpdateMonAsync(global::Monster.Monster request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_UpdateMon, null, options, request);
      }
      public virtual global::Common.ResponseStatus DeleteMon(global::Common.MonId request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return DeleteMon(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Common.ResponseStatus DeleteMon(global::Common.MonId request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_DeleteMon, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Common.ResponseStatus> DeleteMonAsync(global::Common.MonId request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return DeleteMonAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Common.ResponseStatus> DeleteMonAsync(global::Common.MonId request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_DeleteMon, null, options, request);
      }
      /// <summary>Creates a new instance of client from given <c>ClientBaseConfiguration</c>.</summary>
      protected override MonsterServicesClient NewInstance(ClientBaseConfiguration configuration)
      {
        return new MonsterServicesClient(configuration);
      }
    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static grpc::ServerServiceDefinition BindService(MonsterServicesBase serviceImpl)
    {
      return grpc::ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_AddMon, serviceImpl.AddMon)
          .AddMethod(__Method_AddTestMon, serviceImpl.AddTestMon)
          .AddMethod(__Method_LockMonster, serviceImpl.LockMonster)
          .AddMethod(__Method_ReleaseMonster, serviceImpl.ReleaseMonster)
          .AddMethod(__Method_GetMonByID, serviceImpl.GetMonByID)
          .AddMethod(__Method_GetMonsByUserID, serviceImpl.GetMonsByUserID)
          .AddMethod(__Method_UpdateMon, serviceImpl.UpdateMon)
          .AddMethod(__Method_DeleteMon, serviceImpl.DeleteMon).Build();
    }

    /// <summary>Register service method with a service binder with or without implementation. Useful when customizing the  service binding logic.
    /// Note: this method is part of an experimental API that can change or be removed without any prior notice.</summary>
    /// <param name="serviceBinder">Service methods will be bound by calling <c>AddMethod</c> on this object.</param>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static void BindService(grpc::ServiceBinderBase serviceBinder, MonsterServicesBase serviceImpl)
    {
      serviceBinder.AddMethod(__Method_AddMon, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Monster.Monster, global::Common.MonId>(serviceImpl.AddMon));
      serviceBinder.AddMethod(__Method_AddTestMon, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Common.UserId, global::Monster.Monster>(serviceImpl.AddTestMon));
      serviceBinder.AddMethod(__Method_LockMonster, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Monster.LockRequest, global::Monster.Monster>(serviceImpl.LockMonster));
      serviceBinder.AddMethod(__Method_ReleaseMonster, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Monster.ReleaseRequest, global::Common.ResponseStatus>(serviceImpl.ReleaseMonster));
      serviceBinder.AddMethod(__Method_GetMonByID, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Common.MonId, global::Monster.Monster>(serviceImpl.GetMonByID));
      serviceBinder.AddMethod(__Method_GetMonsByUserID, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Common.UserId, global::Monster.MonsterList>(serviceImpl.GetMonsByUserID));
      serviceBinder.AddMethod(__Method_UpdateMon, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Monster.Monster, global::Monster.Monster>(serviceImpl.UpdateMon));
      serviceBinder.AddMethod(__Method_DeleteMon, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Common.MonId, global::Common.ResponseStatus>(serviceImpl.DeleteMon));
    }

  }
}
#endregion
